#!/usr/bin/env bash

cp=$(basename `pwd`)
cpnodash=$(echo $cp | sed 's/-/ /g')

echo "Makeing $cp"

rm -rf content tools
cp -r ../cluster-content/* .
cp ../cluster-content/.gitignore .


find . -type f | grep -v '.git/' | grep -v '.gitlab' | while read f
do
        echo "Processing $f"
        sed -i "s/cluster-content/$cp/g" $f
        sed -i "s/cluster content/$cpnodash/g" $f
        sed -i "s/Cluster Content/$cpnodash/g" $f

        nf=$(echo $f | sed "s/cluster-content/$cp/g")

        mv $f $nf
done

rm -f README.rst
ln -s $cp.rst README.rst
rm tools/clone_content.sh

